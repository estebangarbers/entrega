$(function() {
    var dolar = 42.9;
    $('#PrecioDolar').html("U$S "+dolar);

    ////----- Load lista -----////
    ///
    const app = document.getElementById('productos-list');
    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost/apirest/api/productos', true);
    request.onload = function() {    
    // Begin accessing JSON data here
    var data = JSON.parse(this.response);
    if (request.status >= 200 && request.status < 400) 
    {
        data.forEach(producto => { // iba movie

            const tr = document.createElement('tr');
            tr.setAttribute('id', 'producto' + producto.id);

            const tdid = document.createElement('td');
            tdid.textContent = producto.id;

            const tdnombre = document.createElement('td');
            tdnombre.textContent = producto.nombre;

            const tdprecio = document.createElement('td');
            tdprecio.textContent = producto.precio;

            const tdpreciodolar = document.createElement('td');
            tdpreciodolar.textContent = producto.precio*dolar;

            const editar = document.createElement('td');
            var editarButton = document.createElement('button');
            editarButton.setAttribute('value', producto.id);
            editarButton.setAttribute('type', 'button');
            editarButton.textContent = 'Editar';
            editarButton.className = "btn btn-warning open-modal";

            const eliminar = document.createElement('td');
            var eliminarButton = document.createElement('button');
            eliminarButton.setAttribute('value', producto.id);
            eliminarButton.setAttribute('type', 'button');
            eliminarButton.setAttribute('id', 'delete');
            eliminarButton.textContent = 'Eliminar';
            eliminarButton.className = "btn btn-danger delete";
            
            app.appendChild(tr);
            tr.appendChild(tdid);
            tr.appendChild(tdnombre);
            tr.appendChild(tdprecio);
            tr.appendChild(tdpreciodolar);
            editar.appendChild(editarButton);
            tr.appendChild(editar);
            eliminar.appendChild(eliminarButton);
            tr.appendChild(eliminar);
        });
      }
      else 
      {
        const errorMessage = document.createElement('marquee');
        errorMessage.textContent = 'Recurso no encontrado';
        app.appendChild(errorMessage);
      }
    }

    request.send();


    ////----- CRUD -----////

    ////----- CREATE producto -----////
    $('#btn-add').click(function () {
        $('#btn-save').val("add");
        $('#modalFormData').trigger("reset");
        $('#productoEditorModal').modal('show');
    });
 
    ////----- UPDATE producto -----////
    $('body').on('click', '.open-modal', function () {
        var producto_id = $(this).val();
        $.get('api/productos/' + producto_id, function (data) {
            $('#producto_id').val(data.id);
            $('#nombre').val(data.nombre);
            $('#precio').val(data.precio);
            $('#btn-save').val("update");
            $('#productoEditorModal').modal('show');
        })
    });
 
    // Save CREATE UPDATE
    $("#btn-save").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault();
        var formData = {
            nombre: $('#nombre').val(),
            precio: $('#precio').val(),
        };
        var state = $('#btn-save').val();
        var type = "POST";
        var producto_id = $('#producto_id').val();
        var ajaxurl = 'api/productos';
        if (state == "update") {
            type = "PUT";
            ajaxurl = 'api/productos/' + producto_id;
        }
        $.ajax({
            type: type,
            url: ajaxurl,
            data: formData,
            dataType: 'json',
            success: function (data) {
                var producto_tr = '<tr id="producto' + data.id + '"><td>' + data.id + '</td><td>' + data.nombre + '</td><td>' + data.precio + '</td><td>' + data.precio*dolar + '</td>';
                producto_tr += '<td><button class="btn btn-warning open-modal" value="' + data.id + '">Editar</button></td>';
                producto_tr += '<td><button class="btn btn-danger delete-producto" value="' + data.id + '">Eliminar</button></td></tr>';
                if (state == "add") {
                    $('#productos-list').append(producto_tr);
                } else {
                    $("#producto" + producto_id).replaceWith(producto_tr);
                }
                $('#modalFormData').trigger("reset");
                $('#productoEditorModal').modal('hide');
                
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    ////----- DELETE producto -----////
    $(document).on('click', "button.delete", function() {
        var producto_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "DELETE",
            url: 'api/productos/' + producto_id,
            success: function (data) {
                console.log(data);
                $("#producto" + producto_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
});