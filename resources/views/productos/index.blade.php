@extends('layouts.app')
@section('title')
Inicio
@endsection
@section('extras')
<style type="text/css">
    .table tbody tr td {
        vertical-align: middle;
    }
</style>
@endsection
@section('content')

    @if (session('status'))
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        </div>
    @endif

    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <h2>PRODUCTOS</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Precio ($)</th>
                                <th>Precio (U$S)</th>
                                <th width="1%"></th>
                                <th width="1%"></th>
                            </tr>
                        </thead>
                        <tbody id="productos-list" name="productos-list">
                        </tbody>
                    </table>                        
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="productoEditorModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="productoEditorModalLabel">Producto</h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="row">
                        <form id="modalFormData" name="modalFormData" novalidate="">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::label('nombre', 'Nombre del producto') }}
                                    <div class="form-line">
                                    {{ Form::text('nombre', '', ['class' => 'form-control', 'id' => 'nombre', 'required']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::label('precio', 'Precio del producto') }}
                                    <div class="form-line">
                                    {{ Form::number('precio', '', ['class' => 'form-control', 'id' => 'precio', 'required']) }}
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">                
                    <button type="BUTTON" id="btn-save" class="btn btn-link waves-effect col-green" value="add">GUARDAR CAMBIOS</button>  
                    <button type="button" class="btn btn-link waves-effect pull-left" data-dismiss="modal">CERRAR</button>
                    <input type="hidden" id="producto_id" name="producto_id" value="0">
                </div>
            </div>
        </div>
    </div>
    
@endsection
