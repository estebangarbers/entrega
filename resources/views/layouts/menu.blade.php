    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">RestAPI</a>
            </div>
            <span class="pull-right col-white font-20 m-t-20" id="PrecioDolar"></span>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">NAVEGACIÓN</li>
                    <li>
                        <a href="{{url('/')}}">
                            <i class="material-icons">shopping_cart</i>
                            <span>Productos</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void();" id="btn-add" name="btn-add">
                            <i class="material-icons">add_shopping_cart</i>
                            <span>Nuevo producto</span>
                        </a>
                    </li>                    
                </ul>
            </div>
            <!-- #Menu -->
        </aside>
    </section>