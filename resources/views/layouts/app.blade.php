<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') - RestApi</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('css/all-themes.css')}}" rel="stylesheet" />
    @yield('extras')
</head>

<body class="theme-blue">
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Cargando...</p>
        </div>
    </div>

    <div class="overlay"></div>

    @include('layouts.menu')

    <section class="content">
        <div class="container-fluid">        
            @yield('content')
        </div>
    </section>
    
    <script type="text/javascript" src="{{asset('js/jquery-latest.min.js')}}"></script>
    <!-- Rest -->
    <script src="{{asset('js/rest.js')}}"></script>
    <!-- Bootstrap Core Js -->
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <!-- Custom Js -->
    <script type="text/javascript">
        $(function () {
            setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
        });
    </script>   
    
</body>
</html>