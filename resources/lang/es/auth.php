<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Acceso incorrecto a la cuenta.',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'failed_status' => 'Su cuenta se encuentra inactiva. Por favor confirme su dirección de correo electrónico.',
    'failed_admin_check' => 'Su cuenta todavía no ha sido aprobada por un supervisor.',
];