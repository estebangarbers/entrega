<?php
namespace ApiRest;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
	protected $table = 'productos';

	protected $fillable = ['id','nombre','precio','created_at','updated_at'];
}
