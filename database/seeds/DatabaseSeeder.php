<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(PaisTableSeeder::class);
        $this->call(ProvinciaTableSeeder::class);
        $this->call(PartidoTableSeeder::class);
        $this->call(LocalidadTableSeeder::class);

        $this->call(RubroTableSeeder::class);
    }
}
