<?php
use Illuminate\Http\Request;
use Integral\Producto;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('productos', 'ProductoController@lista');
Route::post('productos', 'ProductoController@store');
Route::get('productos/{producto}', 'ProductoController@show');
Route::put('productos/{producto}', 'ProductoController@update');
Route::delete('productos/{producto}', 'ProductoController@delete');